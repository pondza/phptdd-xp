<?php

use Silex\WebTestCase;

class TestWebRouting extends WebTestCase {

    function __construct() {
        
    }
    public function createApplication() {
         require 'index.php';
         return $app;
    }

    function testInitialPage() {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/v4/api/captcha');
        
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('body'));
        $this->assertRegExp('/[a-zA-Z0-9]* [\-+*] [a-zA-Z0-9]*/',    $client->getResponse()->getContent());
    }

}

?>
