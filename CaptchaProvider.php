<?php 
    class CaptchaProvider{
        private $random;
        
        function __construct() {
            $this->random = new Random();
        }

        function getCaptcha(){
            $pattern = $this->random->next(1, 2);
            $leftOperand = $this->random->next(1, 9);
            $operator = $this->random->next(1, 3);
            $rightOperand = $this->random->next(1, 9);

            return new Captcha($pattern, $leftOperand, $operator, $rightOperand);
        }

        function setRandom(Random $random) {
            $this->random = $random;
        }

    }
 ?>