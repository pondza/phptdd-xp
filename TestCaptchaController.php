<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class TestCaptchaController extends PHPUnit_Framework_TestCase {
    
    function testCaptchaShowCaptcha() {
        
        $captcha = new Captcha(1, 1, 2, 5);
        
        
        $stubCaptchaProvider = $this->getMock('CaptchaProvider');
        $stubCaptchaProvider->expects($this->any())->method('getCaptcha')->will($this->returnValue($captcha));
            
        $captchaController = new CaptchaController();
        $captchaController->setCaptchaProvider($stubCaptchaProvider);
        $this->assertEquals('One * 5' , $captchaController->showCaptcha(new Request(), new Application()));
    }
    
    function testDefaultProviderAtContructor() {
         $captcha = new Captcha(1, 1, 2, 5);        
        $stubCaptchaProvider = $this->getMock('CaptchaProvider');
        $stubCaptchaProvider->expects($this->any())->method('getCaptcha')->will($this->returnValue($captcha));
            
        $captchaController = new CaptchaController($stubCaptchaProvider);
        $this->assertEquals('One * 5' , $captchaController->showCaptcha(new Request(), new Application()));
    }
}
?>
