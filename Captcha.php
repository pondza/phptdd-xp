<?php
class Captcha{
    #panyakorn_pete@hotmail.com
    private $leftOperand;
    private $rightOperand;
    private $operation;
    private $result;
    private $pattern;
    private $operandArray = array(
                    '1' => 'One',
                    '2' => 'Two',
                    '3' => 'Three',
                    '4' => 'Four',
                    '5' => 'Five',
                    '6' => 'Six',
                    '7' => 'Seven',
                    '8' => 'Eight',
                    '9' => 'Nine'
                );
    private $operationArr = array(
                '1' => '+',
                '2' => '*',
                '3' => '-');

    function __construct($pattern, $leftOperand, $operation, $rightOperand){
        
        $this->pattern = $pattern;
        $this->leftOperand = $leftOperand;
        $this->rightOperand = $rightOperand;
        $this->operation = $operation;

        if (!is_numeric($this->pattern) || !is_numeric($this->leftOperand) || !is_numeric($this->operation) || !is_numeric($this->rightOperand)) {
            throw new InvalidArgumentException("Invalid Range Exception", 1);
            
        }
    }

    function getLeftOperand(){
        if($this->pattern == "1"){
            return $this->operandArray[$this->leftOperand];
        }else if($this->pattern == "2"){
            return $this->leftOperand;
        }
    }

    function getOperation(){
        
        return $this->operationArr[$this->operation];
    }

    function getRightOperand(){
        if($this->pattern == "2"){
            return $this->operandArray[$this->rightOperand];
        }else if($this->pattern == "1"){
            return $this->rightOperand;
        }
    }

    function getResult(){

        switch ($this->operation) {
            case '1':
                $result = $this->leftOperand + $this->rightOperand;
                break;
            case '2':
                $result = $this->leftOperand * $this->rightOperand;
                break;
            case '3':
                if (intval($this->leftOperand) < intval($this->rightOperand)) {
                    return 'Invalid Range Exception';
                }
                $result = $this->leftOperand - $this->rightOperand;
                break;
            default : 
        }
        return $result;
    }

    function toString() {
        switch ($this->pattern) {
            case '1':
                return $this->operandArray[$this->leftOperand].' '.$this->operationArr[$this->operation].' '.$this->rightOperand;
                break;

            case '2':
                return $this->leftOperand.' '.$this->operationArr[$this->operation].' '.$this->operandArray[$this->rightOperand];
                break;
        }
    }
}




?>