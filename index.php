<?php
// web/index.php
require_once __DIR__.'/vendor/autoload.php';

$app = new Silex\Application(); 
$app->register(new Silex\Provider\ServiceControllerServiceProvider());

$app['captcha.provider'] = $app->share(function() {
    return new CaptchaProvider;
});

$app['captcha.controller'] = $app->share(function() use ($app) {
    return new CaptchaController($app['captcha.provider']);
});

$app->get('/', function() {
    return "hello";
});

$app->get('/v4/api/captcha', 'captcha.controller:showCaptcha');
$app->run();

?>