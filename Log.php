<?php
Class Log {

    private $pdo;

    function __construct($pdo) {
        $this->pdo = $pdo;
    }

    function countRow() {
        $queryCountRow = "SELECT COUNT(id) FROM access_log;";
        $stm = $this->pdo->prepare($queryCountRow);
        $stm->execute();

        $results = $stm->fetch(PDO::FETCH_NUM);
        return $results[0];
    }

    function createRow($dataLogArray) {
        $insertRow = 'INSERT INTO access_log(' . implode(',', array_keys($dataLogArray)) . ') VALUES("' . implode('","', array_values($dataLogArray)) . '");';
        $stm = $this->pdo->prepare($insertRow);
        $stm->execute();
        return $this->pdo->lastInsertId();
    }
}
?>