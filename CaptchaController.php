<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class CaptchaController {
    
    private $captchaProvider;
    
    function __construct() {
        $this->captchaProvider = new CaptchaProvider();
    }

    public function showCaptcha(Request $request, Application $app) {     
        return $this->captchaProvider->getCaptcha()->toString();
    }
    
    public function setCaptchaProvider(CaptchaProvider $captchaProvider) {
        $this->captchaProvider = $captchaProvider;
    } 

}

?>
